<?php
    $host = rtrim(str_replace('/utils/', '', ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https://" : "http://").$_SERVER['HTTP_HOST'].str_replace(pathinfo(__FILE__, PATHINFO_FILENAME).".php", "", $_SERVER['SCRIPT_NAME'])), '/');
    header('Location: '.$host.'/stats.php');
